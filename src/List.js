import React, { Component } from 'react';
import ListElement from './ListElement';

class App extends Component {
  render() {
    const rows = [];
    this.props.tasks.forEach((task) => {
      rows.push(
        <ListElement
          task={task}
        />
      );
    });

    return (
      <ul className='mainList'>
        {rows}
      </ul>
    );
  }
}

export default App;
