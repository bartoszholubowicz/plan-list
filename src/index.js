import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import List from './List';
import Progress from './Progress';
import registerServiceWorker from './registerServiceWorker';

const LIST = [
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Wstań z łóżka', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Zrób śniadanie', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Umyj się', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Wyjdź na autobus', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Przetrwaj na wykładach', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Wróć do domu', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Zjedz coś', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Poćwicz fizycznie', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Wykąp się', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Zrób po przykładzie z algebry i repety', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Odpocznij', done: false},
    {day: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'], desc: 'Pójdź spać', done: false}
];

ReactDOM.render(<div><List tasks={LIST} /><Progress tasks={LIST} /></div>, document.getElementById('root'));
registerServiceWorker();
