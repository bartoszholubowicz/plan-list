import React, { Component } from 'react';

class Progress extends Component {
  render() {
    const tasksTotal = this.props.tasks.length;
    return (
      <div className='progressBar'>
        {tasksTotal}
      </div>
    );
  }
}

export default Progress;