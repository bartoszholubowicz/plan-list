import React, { Component } from 'react';

class ListElement extends Component {
  constructor(props) {
    super(props);
    this.state = {done: props.task.done};
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const newDone = this.state.done ? false : true;
    this.setState({done: newDone});
  }

  render() {
    const task = this.props.task;
    const desc = this.state.done ?
      <span style={{color: 'green'}}>
        {task.desc}
      </span> :
      task.desc;
    
    if(this.state.done)
      return (
        <li>
          <label>
            {desc} <input type='checkbox' onClick={this.handleClick} checked />
            <span class="checkmark"></span>
          </label>
        </li>
      );
    else
      return (
        <li>
          <label className='container'>
            {desc} <input type='checkbox' onClick={this.handleClick} />
            <span class="checkmark"></span>
          </label>
        </li>
      ); 
  }
}

export default ListElement;